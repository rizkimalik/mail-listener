const fs = require('fs');
const MailListener = require("./MailListener");

let mailListener = [];
const multi = [{
    username: "email.tiketing.selindo@gmail.com",
    password: "dnijfpzpihernjmg",
    host: "imap.gmail.com",
    port: 993,
    tls: true
}, {
    username: "inbound@mendawai.com",
    password: "Invision123!",
    host: "mendawai.com",
    port: 993,
    tls: true
}];

for (let i = 0; i < multi.length; i++) {
    ConnectImap(multi[i].username, multi[i].password, multi[i].host, multi[i].port, multi[i].tls);
}

function ConnectImap(username, password, host, port, tls) {
    if (typeof mailListener[username] != typeof undefined &&
        typeof mailListener[username].state == typeof '' &&
        mailListener[username].state == 'authenticated' &&
        mailListener[username]._config.user == username &&
        mailListener[username]._config.password == password) {

        console.log('IMAP-CLIENT-USE-AUTHENTICATED-CONNECTION ' + username);
    } else {
        port = port || 993;
        tls = tls || true;

        mailListener[username] = new MailListener({
            username: username,
            password: password,
            host: host,
            port: port,
            tls: tls,
            tlsOptions: { rejectUnauthorized: false },
            mailbox: "INBOX",
            markSeen: true,
            fetchUnreadOnStart: true,
            mailParserOptions: { streamAttachments: true },
            attachments: true,
            attachmentOptions: { directory: "attachments/" }
        });

        mailListener[username].start();

        mailListener[username].on("server:connected", function () {
            console.log('IMAP-CLIENT-CONNECTED : ' + username);
        });

        mailListener[username].on("server:disconnected", function () {
            console.log('IMAP-CLIENT-DISCONNECTED : ' + username);
        });

        mailListener[username].on("error", function (err) {
            console.log('IMAP-CLIENT-ERROR : ' + username + ' - ' + err);
        });

        mailListener[username].on("mail", function (mail) {
            console.log(mail);
            logger(mail)
        });

        mailListener[username].on("attachment", function (attachment) {
            console.log(attachment);
            DownloadAttachment(attachment)
            logger(attachment)
        });
    }
}


const logger = (mail) => {
    const directory = `./logs`;
    const filename = Date.now();

    try {
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        fs.appendFile(`${directory}/${filename}.json`, JSON.stringify(mail), function (err) {
            if (err) return console.log(err);
        });
    } catch (err) {
        console.error(err)
    }
}

const DownloadAttachment = (attachment) => {
    const directory = `./attachments/`;
    const filename = attachment.generatedFileName;
    const filestream = directory + filename;

    try {
        if (!fs.existsSync(directory)) {
            fs.mkdirSync(directory, { recursive: true });
        }
        const output = fs.createWriteStream(filestream);
        attachment.stream.pipe(output);
    } catch (err) {
        console.error(err)
    }
}

